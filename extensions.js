const fs = require('fs'); // https://nodejs.org/api/fs.html
const path = require('path');

String.prototype.suffixedIfNotAlready = function(suffix) {
	'use strict';
	if (this.endsWith(suffix)) {
		return this;
	} else {
		return this + suffix.toString();
	}
}

global.asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array)
  }
}

global.createDirIfNotExists = function(dir) {
	if (!fs.existsSync(dir)) {
		fs.mkdirSync(dir);
	}
}

global.fieldDoesNotContainValues = function(record, field, valuesToCheckIfExcludes) {
	return !record[field] || valuesToCheckIfExcludes.every(valueToExclude => !record[field].includes(valueToExclude));
}

global.fieldContainsOneOfValues = function(record, field, valuesToCheckIfIncludes) {
	return record[field] && valuesToCheckIfIncludes.some(valueToInclude => record[field].includes(valueToInclude));
}

global.generateDateStampForFileName = function(date) {
	let year = date.getUTCFullYear().toString();
	let month = (date.getUTCMonth() + 1).toString().padStart(2, '0');
	let day = date.getUTCDate().toString().padStart(2, '0');
	return `${year}${month}${day}`;
}

global.cleanOutputDirectory = function(dir){
   fs.readdir(dir, (err, files) => {
  	if (err) throw err;

	  for (const file of files) {
	    fs.unlink(path.join(dir, file), err => {
	     	if (err) throw err;
	    	});
	  	}
	});
}

module.exports = {};