const fs = require('fs'); // https://nodejs.org/api/fs.html

//global.config = JSON.parse(fs.readFileSync('./config.json')); // contains user input for credentials, dates, etc.
//global.config  = `{"outputDirectory": "./output/","maximumRetryAttemptsOnFail": 2,"maximumActiveOrgs": 1000,"orgs": [{"name": "Reg Org","username": "oceadmin@oceuat.com.reg","password": "crm1OCE3","l$
//require('./config.json');
require('./extensions.js');
require('./Org.js');
require('./OrgManager.js');

require('./Query.js');

global.allOrgs = config.orgs
        .map(function(orgData) {
                let {name, username, password, loginUrl} = orgData;
                return new Org(name, username, password, loginUrl);
        });
module.exports = {};

