const fs = require('fs'); // https://nodejs.org/api/fs.html
const config =JSON.parse(`{"outputDirectory": "./output/","maximumRetryAttemptsOnFail": 2,"maximumActiveOrgs": 1000,"orgs": [{"name": "RV Org","username": "dc@dc.sfconnectordev.com","password": "Imshealth678","loginUrl": "https://login.salesforce.com"}],"queries": [{"name": "Accounts","query": "Select id,name,recordtypeid FROM account limit 10"}]}`);

class OrgManager {
	constructor(orgs, options) {
		this.orgs = orgs;
		this.options = Object.assign({
			maxActiveOrgs: 1000, // max number of concurrent orgs to allow
			maxLoginRetryAttempts: 2
		}, options);
		this.currentActiveOrgs = 0;
		this.retryAttempts = {}; // key is org name, value is number of retry attempts
	}

	async execute(action) {
		await this._execute(this.orgs, action);
	}

	async _execute(orgs, action) {
		await Promise.all(orgs.map(org => new Promise(async (resolve, reject) => {
			let userInfo;
			try {
				userInfo = await org.asyncLogin();
			} catch (error) {
				org.loginFailed = true;
				this.writeErrorFiles(org,error)
				resolve();
				return;
			}
			org.id = userInfo.organizationId;

			try {
				await action(org);
			} catch (error) {
				this.writeErrorFiles(org,error)
				org._errorOccurred(error, "Error during action.", true);
			}
			resolve();
		})));
	}

	showOrgsWithErrors() {
		let orgsWithErrors = this.orgs.filter(org => org.errors.length > 0);
		if (orgsWithErrors.length > 0) {
			console.log(`\n\nThe following orgs had errors which may have prevented successful execution:`);
			orgsWithErrors.forEach(org => console.log(org.name));
		}
	}

	writeErrorFiles(org,error) {
		
		let errorFileContent = `--- ${org.name} ---\n`;
		errorFileContent += `${error.message} | ${error.error}\n\n\n`;

		let directory = config.outputDirectory.suffixedIfNotAlready("/");
		createDirIfNotExists(directory);
		let filename = `${directory}${org.name} Errors.txt`;
		if(!fs.existsSync(filename))
			fs.writeFileSync(filename, errorFileContent);
		else
			fs.appendFileSync(filename, errorFileContent);
			
	}
}

module.exports = global.OrgManager = OrgManager

