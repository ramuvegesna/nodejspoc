const sf = require('node-salesforce'); // https://www.npmjs.com/package/node-salesforce

class Org {
	constructor(name, username, password, loginUrl) {
		this.name = name;
		this.username = username;
		this.password = password;
		this.loginUrl = loginUrl;
		this.connection = new sf.Connection({loginUrl: this.loginUrl});
		this.errors = [];
	}

	_errorOccurred(error, message, log) {
		this.errors.push({error, message});
		if (log) {
			console.log(`${this.name} | ${message} | ${error}`);
		}
	}

	login(onLoggedIn, onError) {
		this.connection.login(this.username, this.password, (error, userInfo) => {
			if (error) {
				this._errorOccurred(error, "Failed to login", true);
				onError();
				return;
			} else {
				onLoggedIn(userInfo, this.connection);
			}
		});
	}

	async asyncLogin() {
		try {
			return await this.connection.login(this.username, this.password);
		} catch (error) {
			this._errorOccurred(error, "Failed to login", true);
			throw error;
		}
	}

	 execute(queryStr) {
		return this.connection.query(queryStr);
	}

	logout(onLoggedOut) {
		this.connection.logout( error => {
			if (error) {
				this._errorOccurred(error, "Failed to logout", true);
			} else {
				(onLoggedOut || function(){console.log(`${this.name} | Logged out.`);})();
			}
		});
	}

	copy() {
		let {name, username, password, loginUrl} = this;
		return new Org(name, username, password, loginUrl);
	}
}

module.exports = global.Org = Org;

